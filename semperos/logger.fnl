(local fun (require :lib.functional))

(fn logger
  [prefix]
  (fn [...]
    (print (.. prefix " " (fun.join " " [...])))))
