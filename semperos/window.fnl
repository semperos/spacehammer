(local view (require :fennelview))
(local {:count count
        :filter filter}
       (require :lib.functional))

(fn move-all-to-other-screen []
  (let [current-screen (hs.screen.mainScreen)
        target-screen (current-screen:previous)
        windows (hs.window.allWindows)]
    (each [_idx window (ipairs windows)]
      (window:moveToScreen target-screen
                           false     ;; allow resizing of windows as they move
                           true))))  ;; ensure window ends up being small enough to fit entirely on new screen

;;;;;;;;;;;;;
;; Exports ;;
;;;;;;;;;;;;;

{:move-all-to-other-screen move-all-to-other-screen}
