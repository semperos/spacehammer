;;;;;;;;;;;;;
;; Exports ;;
;;;;;;;;;;;;;

(local data (require :semperos.midi.data))

(local notes-to-midi data.notes-to-midi)
(local midi-to-notes
       (let [t {}]
         (each [note midi (pairs data.notes-to-midi)]
           (tset t midi note))
         t))

{:notes-to-midi notes-to-midi
 :midi-to-notes midi-to-notes}
