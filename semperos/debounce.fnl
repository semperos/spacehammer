(local view (require :fennelview))

(local logger (require :semperos.logger))
(local log (logger "[DEBOUNCE]"))


(local {:conj conj
        :first first
        :last last
        :take take
        :reverse reverse}
       (require :semperos.util))
(local {:concat concat
        :contains? contains?
        :count count
        :find find
        :merge merge
        :range range}
       (require :lib.functional))

;; Time string to pseudo timestamp
(fn parse-time
  [time-str]
  (let [hour (string.sub time-str 1 2)
        minute (string.sub time-str 4 5)
        second (string.sub time-str 7 8)
        millis (string.sub time-str 10 12)]
    (+ (* 60 60 1000 hour)
       (*    60 1000 minute)
       (*       1000 second)
       (*            millis))))

;; Number of signals within elapsed period of time that must be reached
(local velocity 10)
;; Millis within which velocity must be reached.
;; Low value for higher-intensity, higher value for low-intensity physical action
(local elapsed 10000)

(fn time-based [action-fn dev-entry md]
  (let [knob (. dev-entry.knobs md.controllerNumber)
        times (if knob.static
                  (. knob.times md.controllerValue)
                  (log "Time-based debouncing only implemented for static knob configurations."))
        times (take velocity (reverse (conj times md.timestamp)))]
    (when (>= (length times) velocity)
      (let [[now then] [(first times) (last times)]
            now (parse-time now)
            then (parse-time then)]
        (if (< now then)
            (log "Yesterday passeth, a new day dawneth.")
            (if (<= (- now then) elapsed)
                (do
                  (tset knob.times md.controllerValue [])
                  (action-fn))))))))



;;;;;;;;;;;;;
;; Exports ;;
;;;;;;;;;;;;;

{:time-based time-based}
