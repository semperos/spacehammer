;; Copyright (c) 2020 Daniel Gregoire
;;
;;; Author: Daniel Gregoire
;;
;;; URL: https://gitlab.com/semperos/spacehammer
;;
;;; License: Mozilla Public License Version 2.0
;;

(require-macros :lib.macros)

(local view (require :fennelview))
(local {:concat concat
        :contains? contains?
        :count count
        :find find
        :merge merge
        :range range}
       (require :lib.functional))
(local {:conj conj
        :first first
        :last last
        :reverse reverse
        :take take}
       (require :semperos.util))

(local debounce (require :semperos.debounce))

(local logger (require :semperos.logger))
(local log (logger "[PSALTERY]"))

(local midi (require :semperos.midi))
(local music (require :semperos.music))

(local not-defined :not-defined)

(local {:center-window-frame center-window-frame
        :maximize-window-frame maximize-window-frame
        :move-to-screen move-to-screen
        :resize-half-left resize-half-left
        :resize-half-right resize-half-right
        :resize-inc-left resize-inc-left
        :resize-inc-right resize-inc-right
        :resize-left resize-left
        :resize-right resize-right}
       (require :windows))
(local {:move-all-to-other-screen move-all-to-other-screen}
       (require :semperos.window))

;;;;;;;;;;;;;
;; Devices ;;
;;;;;;;;;;;;;

(local known-devices
       {:atom "ATOM"
        :fighter-twister "Midi Fighter Twister"})

(var act :declare)

(fn ks [...]
  (each [_idx key-stroke (ipairs [...])]
    (act (.. "key/" key-stroke))))

;; TODO This should be done at the knob-turned? function for static knobs
(fn ka [action-fn]
  (fn [dev-entry md]
    (debounce.time-based action-fn dev-entry md)))

(local device-actions
       (let [base {:emacs/save-buffer (fn []
                                        (hs.eventtap.keyStroke [:ctrl] "x")
                                        (hs.eventtap.keyStroke [:ctrl] "s"))
                   :emacs/reload-buffer (fn []
                                          (hs.eventtap.keyStroke [:ctrl] "c")
                                          (hs.eventtap.keyStroke [:ctrl] "k"))
                   :emacs/yank (fn [] (hs.eventtap.keyStroke [:ctrl] "y"))
                   :emacs/kill (fn [] (hs.eventtap.keyStroke [:ctrl] "w"))
                   :emacs/expand-region (fn [] (hs.eventtap.keyStroke [:ctrl] "="))
                   :emacs/kill-sexp-forward (fn [] (hs.eventtap.keyStroke [:option] "f2"))
                   :emacs/kill-sexp-backward (fn [] (hs.eventtap.keyStroke [:option] "f1"))
                   :emacs/transpose-sexps (fn [] (hs.eventtap.keyStroke [:ctrl :option] "t"))
                   :emacs/transpose-sexps-backward (fn [] (hs.eventtap.keyStroke [:option] "f3"))
                   :emacs/barf-forward (fn [] (hs.eventtap.keyStroke [:ctrl :shift] "]"))
                   :emacs/slurp-forward (fn [] (hs.eventtap.keyStroke [:ctrl :shift] "0"))
                   :emacs/splice (fn [] (hs.eventtap.keyStroke [:option] "s"))
                   :emacs/undo (fn [] (hs.eventtap.keyStroke [:shift :ctrl] "-"))
                   :emacs/redo (fn [] (hs.eventtap.keyStroke [:shift :ctrl] "/"))
                   :emacs/previous-line (fn [] (hs.eventtap.keyStroke [:ctrl] "p"))
                   :emacs/next-line (fn [] (hs.eventtap.keyStroke [:ctrl] "n"))
                   :emacs/forward-char (fn [] (hs.eventtap.keyStroke [:ctrl] "f"))
                   :emacs/backward-char (fn [] (hs.eventtap.keyStroke [:ctrl] "b"))
                   :emacs/up-sexp (fn [] (hs.eventtap.keyStroke [:option] "f4")) ;; this is backward up
                   :emacs/down-sexp (fn [] (hs.eventtap.keyStroke [:ctrl :option] "d")) ;; p is backward down
                   :emacs/forward-sexp (fn [] (hs.eventtap.keyStroke [:ctrl :option] "f"))
                   :emacs/backward-sexp (fn [] (hs.eventtap.keyStroke [:ctrl :option] "b"))
                   :window/grow-right  (fn [] (resize-right))
                   :window/grow-left   (fn [] (resize-left))
                   :window/right-half  (fn [] (resize-half-right))
                   :window/left-half   (fn [] (resize-half-left))
                   :window/full-screen (fn [] (maximize-window-frame))
                   :window/center      (fn [] (center-window-frame))
                   :windows/move-all-to-other-screen (fn [] (move-all-to-other-screen))
                   ;; The more convenient Spacehammer functions are broken by recent Hammerspoon,
                   ;; so use the lower-level move-to-screen and screen selection methods.
                   :window/move-north (fn [] (let [focused-window (hs.window.focusedWindow)
                                                   screen (focused-window:screen)]
                                               (move-to-screen (screen:toNorth))))
                   :window/move-south (fn [] (let [focused-window (hs.window.focusedWindow)
                                                   screen (focused-window:screen)]
                                               (move-to-screen (screen:toSouth))))
                   ;; TODO Cover these: http://www.hammerspoon.org/docs/hs.keycodes.html#map
                   :key/enter (fn [] (hs.eventtap.keyStroke [] "return"))
                   :key/down (fn [] (hs.eventtap.keyStroke [] "down"))
                   :key/up (fn [] (hs.eventtap.keyStroke [] "up"))
                   :key/pagedown (fn [] (hs.eventtap.keyStroke [] "pagedown"))
                   :key/pageup (fn [] (hs.eventtap.keyStroke [] "pageup"))
                   ;;
                   ;; == App-specific ==
                   ;;
                   ;;  -- GMail --
                   :gmail/one-touch-archive (fn [] (ks :x :e))
                   :gmail/one-touch-delete (fn [] (ks :x :#))
                   ;; -- Chrome --
                   :chrome/close-tabs-to-the-right (fn [] (when-let [app (: (hs.window.focusedWindow) :application)]
                                                                    (: app :selectMenuItem ["Tab" "Close Tabs to the Right"])))
                   }]         ;; Key strokes for meaningful and valid values that can string.char()
         (for [i 33 126]
           (let [ch (string.char i)
                 modifiers (. {:# [:shift]} ch)
                 key (. {:# "3"} ch)]
             (tset base
                   (.. "key/" ch)
                   (fn [] (hs.eventtap.keyStroke (or modifiers []) (or key ch) 100000)))))
         base))

(set act
     (λ [device-action dev-entry md]
       (let [f (. device-actions device-action)]
         (if f
             (f dev-entry md)
             (log "Device action" device-action "not implemented.")))))

;;;;;;;;;;;;;;;;;;;;;;;
;; Devices and State ;;
;;;;;;;;;;;;;;;;;;;;;;;

(local DEV {})

(fn interval-played? [command-type note-count]
  (and (= 2 note-count)
       (= command-type :noteOff)))

(fn played-notes [notes-played]
  (let [t []]
    (each [note bit (pairs notes-played)]
      (if (= 1 bit)
          (table.insert t note)))
    t))

(fn parse-time
  [time-str]
  (let [hour (string.sub time-str 1 2)
        minute (string.sub time-str 4 5)
        second (string.sub time-str 7 8)
        millis (string.sub time-str 10 12)]
    (+ (* 60 60 1000 hour)
       (*    60 1000 minute)
       (*       1000 second)
       (*            millis))))

;; TODO Allow velocity on a per-controller basis. Small-scale actions can require a lot of knob rotations.
;; Number of signals within elapsed period of time that must be reached
(local velocity 10)
;; Millis within which velocity must be reached.
;; Low value for higher-intensity, higher value for low-intensity physical action
(local elapsed 3000)

;; -- Device Accessors --

(fn knob-entry [dev-entry md]
  (. dev-entry.knobs md.controllerNumber))

(fn btn-entry [dev-entry md]
  (let [btns dev-entry.btns]
    (when btns
      (. btns md.controllerNumber))))

(fn controller-name [controller-entry md]
  (or (and controller-entry controller-entry.name)
      (.. "controller-" md.controllerNumber)))

;; -- Interaction Predicates --

;; TODO Here is where we should be doing the debouncing, if knob is static
(fn knob-turned? [dev-entry command-type md]
  (when (and (= command-type :controlChange)
             (. dev-entry.knobs md.controllerNumber))
    (let [knob (knob-entry dev-entry md)
          value md.controllerValue]
      (if knob.static
          ;; Then: Static knobs only register a turn based on velocity and time elapsed
          (when (or (= value 63)
                    (= value 65))
            (let [times (. knob.times md.controllerValue)
                  times (take velocity (reverse (conj times md.timestamp)))]
              (when (>= (length times) velocity)
                (let [[now then] [(first times) (last times)]
                      now (parse-time now)
                      then (parse-time then)]
                  (if (< now then)
                      ;; Midnight, miss a beat:
                      false
                      ;; Knob turned!
                      (do
                        (tset knob.times md.controllerValue [])
                        (<= (- now then) elapsed)))))))
          ;; Else: Non-static knobs register a turn at every data point:
          true))))

(fn knob-direction-stateful [knob md]
  (let [curr-value md.controllerValue
        prev-value knob.value]
    ;; Allow first turn of knob to set the value
    (if prev-value
        (let [delta (- prev-value curr-value)
              dir (if (< delta 0)
                      :knob-cw
                      :knob-ccw)]
          (tset knob :value curr-value)
          (tset knob :delta delta)
          dir)
        (do
          (tset knob :value curr-value)
          nil))))

(local knob-static-directions
       {65 :knob-cw-static
        63 :knob-ccw-static})

(fn knob-direction [dev-entry md]
  (let [knob (knob-entry dev-entry md)]
    (if knob.static
        (. knob-static-directions md.controllerValue)
        (knob-direction-stateful knob md))))

(fn btn-pressed? [dev-entry command-type md]
  (and (= command-type :controlChange)
       ;; When depressed, a button sends 127. When lifted, 0.
       (= 0 md.controllerValue)))

;; -- Command Callbacks --

(fn synthesize-command [dev-entry command-type md]
  (let [notes-played dev-entry.notes-played
        note-count (count (played-notes notes-played))]
    (if (interval-played? command-type note-count)
        :interval-played
        (knob-turned? dev-entry command-type md)
        (knob-direction dev-entry md)
        (btn-pressed? dev-entry command-type md)
        :btn-pressed
        command-type)))

(fn note-action [dev-entry note-name]
  (let [notes dev-entry.notes
        midi-value (. midi.notes-to-midi note-name)
        action (. notes midi-value)]
    (or action not-defined)))

(local atom-callbacks
       {:noteOff (fn [dev-entry md]
                   (let [note-name (. midi.midi-to-notes md.note)
                         notes-played dev-entry.notes-played
                         chord dev-entry.chord]
                     (if (not chord.played?)
                         (let [action (note-action dev-entry note-name)]
                           (if (= action not-defined)
                               (log "[_]" note-name)
                               (act action dev-entry md))

                           (tset chord :played? true)))
                     (tset notes-played note-name 0)))})

(fn cb-interval-played [dev-entry md]
  (let [notes-played dev-entry.notes-played
        [midi-value-a midi-value-b] (let [t []]
                                      (each [note bit (pairs notes-played)]
                                        (if (= 1 bit)
                                            (table.insert t (. midi.notes-to-midi note))))
                                      t)
        chord dev-entry.chord]
    (if (not chord.played?)
        (let [action :tbd]
          (log "[♫]" (music.interval-name (music.interval midi-value-a midi-value-b)))
          (tset chord :played? true)
          ;; We invoke this because we've hijacked noteOff to detect the interval.
          (atom-callbacks.noteOff dev-entry md)))))

(fn cb-noteOn [dev-entry md]
  (let [note-name (. midi.midi-to-notes md.note)
        notes-played dev-entry.notes-played
        chord dev-entry.chord]
    (if chord.played?
        (tset chord :played? false))
    (tset notes-played note-name 1)
    (log "[♩]" note-name)))

(fn cb-knob-cw [dev-entry md]
  (let [knob (knob-entry dev-entry md)
        action (or knob.cw not-defined)
        amount (or knob.delta "N/A")]
    (if (= action not-defined)
        (log (string.format "[🎛 %s]" knob.name) "↷" amount action)
        (act action dev-entry md))))

(fn cb-knob-ccw [dev-entry md]
  (let [knob (knob-entry dev-entry md)
        action (or knob.ccw not-defined)
        amount (or knob.delta "N/A")]
    (if (= action not-defined)
        (log (string.format "[🎛 %s]" knob.name) "↶" amount action)
        (act action dev-entry md))))

(fn cb-btn-pressed [dev-entry md]
  (let [btn (btn-entry dev-entry md)
        action (or (and btn btn.press) not-defined)
        name (controller-name btn md)]
    (if (= action not-defined)
        (log "[🔘 pressed]" name action)
        (act action dev-entry md))))

(local commands
       ;; Order Matters
       [[:interval-played cb-interval-played]
        [:noteOn          cb-noteOn]
        [:noteOff         atom-callbacks.noteOff]
        [:knob-cw         cb-knob-cw]
        [:knob-ccw        cb-knob-ccw]
        [:knob-cw-static  cb-knob-cw]
        [:knob-ccw-static cb-knob-ccw]
        [:btn-pressed     cb-btn-pressed]])

(fn callback-atom [obj _device-name command-type desc md]
  "Device callback for the PreSonus ATOM"
  (let [dev-entry DEV.atom
        command (synthesize-command dev-entry command-type md)
        command-entry (find (fn [[tag _]] (= tag command)) commands)]
    (when command-entry
      (let [[_ command-fn] command-entry]
        (command-fn dev-entry md))))
  ;; Command types:
  ;; :channelPressure ;; :pressure
  )

;; TODO There's a more optimal entry function for all devices that at
;; least logs activity before wiring up the device specifically.
(fn callback-fighter-twister [obj _device-name command-type desc md]
  "Device callback for the DJTechTools MIDI Fighter Twister"
  (let [dev-entry DEV.fighter-twister
        command (synthesize-command dev-entry command-type md)
        command-entry (find (fn [[tag _]] (= tag command)) commands)]
    (when command-entry
      (let [[_ command-fn] command-entry]
        (command-fn dev-entry md))))
  )

(local device-callbacks
       {:atom callback-atom
        :fighter-twister callback-fighter-twister})

;; Controller changes, not note changes
(local atom-controller-keymap
       (let [[knob-1 knob-2 knob-3 knob-4] [14 15 16 17]
             [nav-up nav-down] [87 89]
             [nav-left nav-right] [90 102]
             [slct zoom] [103 104]]
         {:knobs {knob-1 {:name :knob-1
                          :cw :chrome/close-tabs-to-the-right}
                  knob-2 {:name :knob-2}
                  knob-3 {:name :knob-3
                          :cw :window/grow-right
                          :ccw :window/grow-left}
                  knob-4 {:name :knob-4}}
          :btns {nav-up {:name :nav-up
                         :press :window/move-north}
                 nav-down {:name :nav-down
                           :press :window/move-south}
                 nav-left {:name :nav-left
                           :press :window/left-half}
                 nav-right {:name :nav-right
                            :press :window/right-half}
                 slct {:name :select
                       :press :window/full-screen}
                 zoom {:name :zoom
                       :press :windows/move-all-to-other-screen}}
          :notes (let [notes {;; GREEN BANK. Designed for use in GMail.
                              :G4  :key/k
                              :D#4 :key/j
                              :Gb4 :key/x
                              :F4  :key/e ;; 14 Tempo pad
                              :C#4 :key/# ;; 10 Delete pad
                              :D4  :key/enter
                              :B3  :key/up
                              :G3  :key/down
                              :Bb3 :key/pageup
                              :Gb3 :key/pagedown
                              :E4  :gmail/one-touch-archive
                              :C4  :gmail/one-touch-delete
                              }
                       t {}]
                   (each [note-name action (pairs notes)]
                     (tset t (. midi.notes-to-midi note-name) action))
                   t)
          :intervals {}
          ;; A chord is a combination of any controls, but especially notes.
          ;; When an action is performed for a chord, the chord has been "played"
          ;; and all follow-on signals (e.g., noteOff as you let go of the notes)
          ;; are disregarded.
          :chord {:played? false}}))

(fn static-knob [m]
  (let [[ccw cw] [63 65]]
    (merge m {:static true :times {cw [] ccw []}})))

(local fighter-twister-controller-keymap
       ;; Knobs are 0 through 63, 4 banks of 16 knobs
       ;;
       ;; A static knob sends 65 for cw, 63 for ccw
       (let [default-knobs {}
             _ (for [idx 0 15]  ;; eventually all 63
                 (tset default-knobs idx {:name (string.format "knob-%d" idx)}))]
         {:knobs (merge default-knobs
                        {0 (static-knob
                            {:name "knob-emacs-undo-redo"
                             :cw :emacs/redo
                             :ccw :emacs/undo})
                         1 (static-knob
                            {:name "knob-kill-ring"
                             :cw :emacs/yank})
                         2 (static-knob
                            {:name "knob-kill-and-region"
                             :cw :emacs/expand-region
                             :ccw :emacs/kill})
                         3 (static-knob
                            {:name "knob-emacs-kill"
                             :cw :emacs/kill-sexp-forward
                             :ccw :emacs/kill-sexp-backward})
                         8 (static-knob
                            {:name "knob-emacs-up-down-sexp"
                             :cw :emacs/down-sexp
                             :ccw :emacs/up-sexp})
                         9 (static-knob
                            {:name "knob-emacs-foward-backward-sexp"
                             :cw :emacs/forward-sexp
                             :ccw :emacs/backward-sexp})
                         11 (static-knob
                             {:name "knob-emacs-transpose-sexps"
                              :cw :emacs/transpose-sexps
                              :ccw :emacs/transpose-sexps-backward})
                         12 (static-knob
                             {:name "knob-emacs-nav-lines"
                              :cw :emacs/next-line
                              :ccw :emacs/previous-line})
                         13 (static-knob
                             {:name "knob-emacs-nav-chars"
                              :cw :emacs/forward-char
                              :ccw :emacs/backward-char})
                         14 (static-knob
                             {:name "knob-emacs-splice"
                              :cw :emacs/splice
                              :ccw :emacs/undo})
                         15 (static-knob
                             {:name "knob-emacs-slurpin"
                              :cw :emacs/slurp-forward
                              :ccw :emacs/barf-forward})
                         27 (static-knob
                             {:name "knob-window-full-or-center"
                              ;; :cw :window/center
                              :cw :window/full-screen
                              :ccw :window/full-screen})
                         30 (static-knob
                             {:name "knob-window-switch-monitors"
                              :cw :window/move-south
                              :ccw :window/move-north})
                         31 (static-knob
                             {:name "knob-window-left-right"
                              :cw :window/right-half
                              :ccw :window/left-half})})
          :btns {0 {:name "btn-emacs-save"
                    :press :emacs/save-buffer}
                 1 {:name "btn-emacs-reload"
                    :press :emacs/reload-buffer}}}))

(local controller-keymaps
       {:atom atom-controller-keymap
        :fighter-twister fighter-twister-controller-keymap})

(fn register-device [name midi-obj]
  (let [entry (merge {:obj midi-obj
                      :name name
                      ;; Can make a [] if order comes to matter.
                      :notes-played {}}
                     (. controller-keymaps name))]
    (tset DEV name entry)))

(λ register-devices [?connected-devices]
  (let [connected-devices (or ?connected-devices
                              (hs.midi:devices))]
    (each [kw-name device-name (pairs known-devices)]
      (each [idx connected-device (ipairs connected-devices)]
        (when (= connected-device device-name)
          (let [midi-obj (hs.midi.new device-name)]
            (midi-obj:callback (. device-callbacks kw-name))
            (register-device kw-name midi-obj)))))))

;;;;;;;;;;;;;
;; Exports ;;
;;;;;;;;;;;;;

{:register-devices register-devices
 :devices DEV}
