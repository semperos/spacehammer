;; From Spacehammer
(fn slice-start-end
  [start end list]
  (let [end+ (if (< end 0)
                 (+ (length list) end)
                 end)]
    (var sliced [])
    (for [i start end+]
      (table.insert sliced (. list i)))
    sliced))

(fn last
  [list]
  (. list (length list)))

;; From Spacehammer
(fn drop
  [n list]
  (slice-start-end n (length list) list))

(fn take
  [n list]
  (slice-start-end 0 n list))

(fn conj
  [tbl item]
  (table.insert tbl item)
  tbl)

(fn first
  [list]
  (. list 1))

;; From Spacehammer
(fn last
  [list]
  (. list (length list)))

(fn reverse [list]
  (let [return []
        count (length list)]
    (for [i count 1 -1]
      (table.insert return (. list i)))
    return))

;; Quick and dirty multimethods from Toby Crawl
(local _mutlifn-dispatch-table {})

(macro multifn [name dispatch-fn]
  `(fn ,name [...]
     (let [dispatch-v# (,dispatch-fn ...)
           f-table# (. _mutlifn-dispatch-table ,name)
           f# (or (. f-table# dispatch-v#)
                  (. f-table# :default))]
       (f# ...))))

(fn multi [name dispatch-v f]
  (let [f-table (or (. _mutlifn-dispatch-table name)
                    {})]
    (tset f-table dispatch-v f)
    (tset _mutlifn-dispatch-table name f-table)
    nil))

;;;;;;;;;;;;;
;; Exports ;;
;;;;;;;;;;;;;
{:conj conj
 :drop drop
 :first first
 :last last
 :reverse reverse
 :take take}
