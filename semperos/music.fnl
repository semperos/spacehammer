;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; General Music Utilities ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(local intervals
       {0
        {:std-interval :perfect-unison
         :std-short    :P1
         :dim-interval :diminished-second
         :dim-short    :d2}
        1
        {:std-interval :minor-second
         :std-short    :m2
         :aug-interval :augmented-unison
         :aug-short    :A1}
        2
        {:std-interval :major-second
         :std-short :M2
         :aug-interval :diminished-third
         :aug-short :d3}
        3
        {:std-interval :minor-third
         :std-short :m3
         :aug-interval :augmented-second
         :aug-short :A2}
        4
        {:std-interval :major-third
         :std-short :M3
         :aug-interval :diminished-fourth
         :aug-short :d4}
        5
        {:std-interval :perfect-fourth
         :std-short :P4
         :aug-interval :augmented-third
         :aug-short :A3}
        6
        {:dim-interval :diminished-fifth
         :dim-short :d5
         :aug-interval :augmented-fourth
         :aug-short :A4}
        7
        {:std-interval :perfect-fifth
         :std-short :P5
         :aug-interval :diminished-sixth
         :aug-short :d6}
        8
        {:std-interval :minor-sixth
         :std-short :m6
         :aug-interval :augmented-fifth
         :aug-short :A5}
        9
        {:std-interval :major-sixth
         :std-short :M6
         :aug-interval :diminished-seventh
         :aug-short :d7}
        10
        {:std-interval :minor-seventh
         :std-short :m7
         :aug-interval :augmented-sixth
         :aug-short :A6}
        11
        {:std-interval :major-seventh
         :std-short :M7
         :aug-interval :diminished-octave
         :aug-short :d8}
        12
        {:std-interval :perfect-octave
         :std-short :P8
         :aug-interval :augmented-seventh
         :aug-short :A7}})

(fn interval [note-a note-b]
  (math.abs (- note-a note-b)))

(fn interval-name [num-semitones]
  (let [interval-entry (. intervals num-semitones)
        std interval-entry.std-short]
    (if std
        std
        interval-entry.aug-short)))

;;;;;;;;;;;;;
;; Exports ;;
;;;;;;;;;;;;;

{:interval interval
 :interval-name interval-name
 :intervals intervals}
