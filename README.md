# Spacehammer Environment

_NB: The combination of Hammerspoon and Spacehammer will not reload the Fennel files in `~/.spacehammer` if they are symlinks, so this is stored as a separate repo from [my config](https://gitlab.com/semperos/config)._

[Spacehammer](https://github.com/agzam/spacehammer) wires up Fennel for interacting with Hammerspoon's Lua API and provides a number of facilities on top of default Hammerspoon.

## MIDI Controllers

Goal: Use MIDI controllers for more intuitive and less physically taxing interaction at the computer.

### Data Model

Both available MIDI data and ergonomics of different MIDI instruments need to be considered.

MIDI controllers produce note data as well as other "signals" like pitch wheel and ad hoc controller slots (e.g., that knobs on an instrument might control).

To gain the most _granularity_, we want to model things based on known instruments. This allows us not to have to fiddle with what octave an instrument is on or whether its knobs behave differently than other instruments.

See `psaltery.fnl` for the instrument interface and `midi.fnl` for MIDI utilities.

### Behavior Model

#### Debouncing/Pacing

Knobs provide a natural control for moving things on the screen. However, knobs tend to provide one of the following streams of data when used:

* Traditional knob: Values from 0 to 127, stopping when reaching either the minimum or the maximum (even if the physical knob is "endless")
  * Some devices provide a relatively simple way to reset a knob.
* ENC 3FH/41H, where values 65 and 63 are produced for clockwise and counter-clockwise rotations, respectively.

I implemented a function to trigger custom behavior only when a knob has traversed at least N data points in a particular direction. This solved the issue of controls being too sensitive to use for imprecise human hands, but did not solve the "reached the maximum, now have to rotate counter-clockwise and then clockwise again," a problem which also made it tough to assign different behaviors to cw and ccw directions.

This could now be solved that I've obtained a Midi Fighter Twister, since its knobs can be programmed to reset the value of the knob when pressed. This, however, introduces weird friction to its use.

The other way to pace these inputs would be time-based. The metadata has a timestamp, so using this with the ENC 3FH/41H encoder type via the Fighter Twister should be feasible without needing to reset the knob with a button press.

### Devices

#### Presonus Atom

* Nice assortment of pads and buttons with numbers and words which one can use
  to help remember what pads are assigned to what.

#### Midi Fighter Twister

* Highly customizable via Windows/macOS utility program.

## License

Code in `config.fnl` from Spacehammer:

> Copyright (c) 2017-2020 Ag Ibragimov & Contributors
>
> Author: Ag Ibragimov <agzam.ibragimov@gmail.com>
>
> Contributors:
>   Jay Zawrotny <jayzawrotny@gmail.com>
>
> URL: https://github.com/agzam/spacehammer
>
> License: MIT

All other code in this repository:

Copyright (c) 2020 Daniel Gregoire

Licensed under the [Mozilla Public License Version 2.0](https://www.mozilla.org/en-US/MPL/2.0/).
